﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	private Vector2 heading = Vector2.right;

	void Update()
	{
		//calculate the closest player
		float playerDistance = Vector2.Distance(target.position, transform.position);
		float player2Distance = Vector2.Distance(target2.position, transform.position);

		Vector2 direction;
		if(playerDistance < player2Distance)
		{
			// get the vector from the bee to the target 
			direction = target.position - transform.position;
		}
		else
		{
			direction = target2.position - transform.position;
		}

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading))
		{
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else
		{
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos()
	{
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);

		// draw target vector in green
		Gizmos.color = Color.green;
		Vector2 direction2 = target2.position - transform.position;
		Gizmos.DrawRay(transform.position, direction2);
	}
}
