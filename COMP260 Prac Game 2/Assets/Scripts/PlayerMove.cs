﻿using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public string horizontalAxis;
	public string verticalAxis;

	void Update()
	{
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis(verticalAxis);

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}
